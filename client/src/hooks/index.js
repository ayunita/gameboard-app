import { useState, useEffect } from "react";
import historyService from "../services/history";
import { WebsocketListener } from "../services/ws-live";
import { createPlayerData, createFullGameData } from "../utils/app_helper";

export const useGameData = () => {
  const [history, setHistory] = useState([]);
  const [live, setLive] = useState();
  const [currentGames, setCurrentGames] = useState([]);
  const [pastGames, setPastGames] = useState([]);

  // Listens to web socket
  WebsocketListener(setLive);

  // Updates the current and past game list everytime there is a new live game
  useEffect(() => {
    if (live) {
      if (live.type === "GAME_BEGIN") {
        // If it is a "GAME_BEGIN", then add the game into the current game list
        const player = createPlayerData(live);
        setCurrentGames([player, ...currentGames]);
      } else {
        // If it is a "GAME_RESULT", then remove the current game from the current game list
        // and add it into past game list
        const updatedGames = currentGames.filter(
          (g) => g.gameId !== live.gameId
        );
        setCurrentGames(updatedGames);
        const pastGame = createFullGameData(live);
        setPastGames([pastGame, ...pastGames]);
      }
    }
    return () => {};
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [live]);

  // Retrieves all history data when the component first loads
  useEffect(() => {
    let temp = [];
    historyService.getAll().then((res) => {
      res.data.forEach((r) => {
        temp.push(createFullGameData(r));
      });
      setHistory(temp.sort((a, b) => new Date(b.time) - new Date(a.time)));
    });
  }, []);

  const resource = {
    history,
    currentGames,
    pastGames,
  };

  return resource;
};

export const useModal = () => {
  const [visible, setVisibility] = useState(false);
  const [content, setContent] = useState(null);
  const open = () => setVisibility(true);
  const close = () => setVisibility(false);
  const set = (newContent) => setContent(newContent);
  return {
    visible,
    content,
    set,
    open,
    close,
  };
};
