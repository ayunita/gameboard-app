import React from "react";
import Typography from "@mui/material/Typography";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Grid from "@mui/material/Grid";
import { purple } from "@mui/material/colors";

const CurrentGameList = ({ data }) => {
  return (
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <Typography variant="h6">Current Games</Typography>
      </Grid>
      {data.map((g, i) => (
        <Grid key={`${g.gameId}_${i}`} item xs={4}>
          <Card sx={{ background: purple[700] }}>
            <CardContent sx={{ textAlign: "center" }}>
              <Typography fontWeight="bold">
                {g.playerA} VS {g.playerB}
              </Typography>
            </CardContent>
          </Card>
        </Grid>
      ))}
    </Grid>
  );
};

export default CurrentGameList;
