import React from "react";
import {textToEmoji} from "../utils/app_helper";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Modal from "@mui/material/Modal";
import Grid from "@mui/material/Grid";

const PlayerModal = ({ visible, onClose, content }) => {
  if (!content) return <></>;

  const modalStyle = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 400,
    bgcolor: "background.paper",
    border: "2px solid #000",
    boxShadow: 24,
    p: 4,
  };

  return (
    <Modal open={visible} onClose={onClose}>
      <Box sx={modalStyle}>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <Typography variant="h6">{content.player}</Typography>
            <Typography variant="subtitle2">
              {new Date().toLocaleString()}
            </Typography>
          </Grid>
          <Grid item xs={6}>
            <Typography>Past games: {content.totalGames}</Typography>
            <Typography>Win: {content.results.win}</Typography>
            <Typography>Draw: {content.results.draw}</Typography>
            <Typography>Loss: {content.results.loss}</Typography>
          </Grid>
          <Grid item xs={6}>
          <Typography>Most played: {textToEmoji(content.played.fav.toUpperCase())}</Typography>
            <Typography>{textToEmoji("ROCK")}: {content.played.rock}</Typography>
            <Typography>{textToEmoji("PAPER")}: {content.played.paper}</Typography>
            <Typography>{textToEmoji("SCISSORS")}: {content.played.scissors}</Typography>
          </Grid>
        </Grid>
      </Box>
    </Modal>
  );
};

export default PlayerModal;
