export const resultRatio = (gamesA, gamesB) => {
  const winA = gamesA.filter((g) => g.winner === g.playerA).length;
  const drawA = gamesA.filter((g) => g.winner === "Draw").length;
  const winB = gamesB.filter((g) => g.winner === g.playerB).length;
  const drawB = gamesB.filter((g) => g.winner === "Draw").length;
  return {
    win: winA + winB,
    draw: drawA + drawB,
    loss: gamesA.length + gamesB.length - (winA + winB + drawA + drawB),
  };
};

export const totalGames = (gamesA, gamesB) => {
  return gamesA.length + gamesB.length;
};

export const handPlayed = (gamesA, gamesB) => {
  const rockA = gamesA.filter((g) => g.playedA === "ROCK").length;
  const rockB = gamesB.filter((g) => g.playedB === "ROCK").length;
  const paperA = gamesA.filter((g) => g.playedA === "PAPER").length;
  const paperB = gamesB.filter((g) => g.playedB === "PAPER").length;
  const scissorsA = gamesA.filter((g) => g.playedA === "SCISSORS").length;
  const scissorsB = gamesB.filter((g) => g.playedB === "SCISSORS").length;

  const rock = rockA + rockB;
  const paper = paperA + paperB;
  const scissors = scissorsA + scissorsB;
  const results = { rock, paper, scissors };
  const fav = Object.keys(results).reduce((a, b) =>
    results[a] > results[b] ? a : b
  );
  return {
    ...results,
    fav,
  };
};
