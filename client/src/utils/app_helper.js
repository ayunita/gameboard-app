export const createPlayerData = (data) => {
  return {
    gameId: data.gameId,
    playerA: data.playerA.name,
    playerB: data.playerB.name,
  };
};

export const createFullGameData = (data) => {
  const winner = getWinner(data.playerA, data.playerB);
  return {
    gameId: data.gameId,
    time: new Date(data.t).toLocaleString(),
    playerA: data.playerA.name,
    playedA: data.playerA.played,
    playerB: data.playerB.name,
    playedB: data.playerB.played,
    winner: winner
  };
};

export const textToEmoji = (hand) => {
  switch (hand) {
    case "ROCK":
      return "✊";
    case "PAPER":
      return "🖐";
    case "SCISSORS":
      return "✌";
    default:
      return "";
  }
};

const getWinner = (playerA, playerB) => {
  if (playerA.played === "PAPER" && playerB.played === "ROCK")
    return playerA.name;
  else if (playerA.played === "SCISSORS" && playerB.played === "PAPER")
    return playerA.name;
  else if (playerA.played === "ROCK" && playerB.played === "SCISSORS")
    return playerA.name;
  else if (playerB.played === "PAPER" && playerA.played === "ROCK")
    return playerB.name;
  else if (playerB.played === "SCISSORS" && playerA.played === "PAPER")
    return playerB.name;
  else if (playerB.played === "ROCK" && playerA.played === "SCISSORS")
    return playerB.name;
  else return "Draw";
};