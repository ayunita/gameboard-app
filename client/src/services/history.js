import axios from "axios";
const baseUrl = "/api/history";

const getAll = async () => {
  const response = await axios.get(baseUrl);
  return response.data;
};

const methods = { getAll }
export default methods