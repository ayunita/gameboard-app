import { useRef, useEffect } from "react";
const baseUrl = process.env.REACT_APP_WS_BASE_URL;

export const WebsocketListener = (onMessage) => {
  const ws = useRef(null);

  // Initialize web socket
  useEffect(() => {
    ws.current = new WebSocket(`${baseUrl}/rps/live`);
    ws.current.onopen = () => console.info("Web socket is open");
    ws.current.onmessage = (e) => {
      // The first parsing is to convert string into json string
      // The second parsing is to convert json string into json object
      const data = JSON.parse(JSON.parse(e.data));
      onMessage(data);
    };
    ws.current.onclose = () => console.info("Web socket is closed");

    const wsCurrent = ws.current;
    return () => {
      wsCurrent.close();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
};
