import React from "react";
import { useGameData, useModal } from "./hooks";
import { totalGames, handPlayed, resultRatio } from "./utils/stats_helper";

import GameTable from "./components/GameTable";
import CurrentGameList from "./components/CurrentGameList";
import PlayerModal from "./components/PlayerModal";

import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";

const fullGamesColumns = [
  { id: "time", label: "Datetime" },
  { id: "playerA", label: "Player A" },
  { id: "playedA", label: "A Played" },
  { id: "playerB", label: "Player B" },
  { id: "playedB", label: "B Played" },
];

const App = () => {
  const data = useGameData();
  const { history, currentGames, pastGames } = data;

  const modal = useModal();
  const showPlayerInformation = (name) => {
    const allPastGames = history.concat(pastGames);
    let gamesA = allPastGames.filter((g) => g.playerA === name);
    let gamesB = allPastGames.filter((g) => g.playerB === name);

    const content = {
      player: name,
      totalGames: totalGames(gamesA, gamesB),
      played: handPlayed(gamesA, gamesB),
      results: resultRatio(gamesA, gamesB),
    };
    modal.set(content);
    modal.open();
  };

  return (
    <Box sx={{ p: 4 }}>
      <PlayerModal
        visible={modal.visible}
        onClose={modal.close}
        content={modal.content}
      />
      <Typography variant="h5">Rock-Paper-Scissors Gameboard</Typography>
      <Box sx={{ mt: 4, mb: 4 }}>
        <CurrentGameList data={currentGames} />
      </Box>
      <Box sx={{ mt: 4, mb: 4 }}>
        <GameTable
          title="Past Games"
          columns={fullGamesColumns}
          rows={pastGames}
          playerClicked={showPlayerInformation}
        />
      </Box>
      <Box sx={{ mt: 4, mb: 4 }}>
        <GameTable
          title="Game History"
          columns={fullGamesColumns}
          rows={history}
          playerClicked={showPlayerInformation}
        />
      </Box>
    </Box>
  );
};

export default App;
