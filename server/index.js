require("dotenv").config();
const express = require("express");
const axios = require("axios");
const cors = require("cors");

const app = express();
app.use(cors());

const baseUrl = process.env.HTTP_BASE_URL;

/** Requests data recursively from the original api if there is a cursor pagination.
 *  @param {string} cursor  pointer to another dataset
 *  @param {Array} data     collection of history data
 *  @param {Number} count   the number of calls, the default is 2
 *  @return {Object}        the next cursor and accumulated history data
 */
const getHistory = async (cursor, data = [], count = 2) => {
  try {
    const result = await axios.get(`${baseUrl}${"" || cursor}`);
    if (result.data.length < 1 || count === 0) return { cursor, data };
    const updatedData = data.concat(result.data.data);
    const nextCursor = result.data.cursor;
    const updatedCount = count - 1;
    return getHistory(nextCursor, updatedData, updatedCount);
  } catch (e) {
    console.error("getHistory error", e);
  }
};

app.get("/api/history", (request, response) => {
  const cursor = request.params.cursor || "";
  const count = request.params.count;
  getHistory(`/rps/history${cursor}`, [], count).then((res) => {
    response.json(res);
  });
});

const PORT = 3001;
app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});
