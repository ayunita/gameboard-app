# Rock-Paper-Scissors (RPS) Gameboard App

RPS Gameboard App displays rock-paper-scissors ongoing matches and historical results of individual players. The App powered by React (front-end) and NodeJS/Express (back-end).

**LIVE DEMO**: [https://rps-gameboard.herokuapp.com/](https://rps-gameboard.herokuapp.com/)

*Notes: 

- due to excessive historical data, the default of historical data is 2 pages (api calls).
- server is built due to cors issue. Other alternative is by using [cors anywhere](https://cors-anywhere.herokuapp.com/corsdemo) (not for production)

## Installation

**Prerequisites:** your machine environment must have [node](https://nodejs.org/en/) and [npm](https://docs.npmjs.com/getting-started).

This app is divided into two components: client (front-end) and server (back-end). 

To run this app on your local environment, please follow these steps:

### Client
1. Install all dependencies
```
cd client && npm install
```

2. Add `.env` under client's root, and enter this following information:
```
REACT_APP_WS_BASE_URL=<wss://something.here.com>
```

3. Start the app
```
npm run start
```

### Server
1. Install all dependencies
```
cd server && npm install
```

2. Add `.env` under server's root, and enter this following information:
```
HTTP_BASE_URL=<http://something.here.com>
```

3. Start the server
```
npm run start
```

## Author
Andriani Yunita
